package main

import (
	"context"
	"fmt"
	"io"
	"time"

	pb "gitlab.com/michalmajewskiand/grpc-todolist/proto"
	"google.golang.org/grpc"
)

const (
	address = "localhost:50051"
)

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		fmt.Println("did not connect: %v", err)
	}
	defer conn.Close()
	client := pb.NewReminderServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	stream, err := client.ListReminders(ctx, &pb.ListRemindersReq{User: "Miguelito"})

	if err != nil {
		fmt.Println("Error: %v", err)
	}

	for {
		// stream.Recv returns a pointer to a ListReminders at the current iteration
		res, err := stream.Recv()
		// If end of stream, break the loop
		if err == io.EOF {
			break
		}
		// if err, return an error
		if err != nil {
			fmt.Println("Error: %v", err)
		}
		// If everything went well use the generated getter to print the reminder
		fmt.Println(res)
	}
	return

}
