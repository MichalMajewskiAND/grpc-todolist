package test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/golang/protobuf/proto"

	pb_mock "gitlab.com/michalmajewskiand/grpc-todolist/mocks"
	pb "gitlab.com/michalmajewskiand/grpc-todolist/proto"
)

var reminder = &pb.Reminder{
	Id:       "74e07af2-18f4-49c9-9cd4-84e0c1d06b6f",
	User:     "Miguelito",
	DueDate:  "2019-08-30T11:29:16Z",
	Finished: false,
	Action:   "Test action",
}

func TestListReminders(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create mock for the stream returned by RouteChat
	stream := pb_mock.NewMockReminderService_ListRemindersClient(ctrl)

	// set expectation on sending.
	stream.EXPECT().SendMsg(gomock.Any()).Return(nil)

	// Set expectation on receiving.
	stream.EXPECT().Recv().Return(reminder, nil)
	stream.EXPECT().CloseSend().Return(nil)

	// Create mock for the client interface.

	client := pb_mock.NewMockReminderServiceClient(ctrl)

	client.EXPECT().ListReminders(gomock.Any(), &pb.ListRemindersReq{User: "Miguelito"}).Return(stream, nil)

	if err := testListReminders(client); err != nil {
		t.Fatalf("Test failed: %v", err)
	}
}

func testListReminders(client pb.ReminderServiceClient) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	stream, err := client.ListReminders(ctx, &pb.ListRemindersReq{User: "Miguelito"})
	if err != nil {
		return err
	}
	if err := stream.SendMsg(reminder); err != nil {
		return err
	}
	if err := stream.CloseSend(); err != nil {
		return err
	}
	got, err := stream.Recv()
	if err != nil {
		return err
	}
	if !proto.Equal(got, reminder) {
		return fmt.Errorf("stream.Recv() = %v, want %v", got, reminder)
	}
	return nil
}

func TestCreateReminder(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	client := pb_mock.NewMockReminderServiceClient(ctrl)

	client.EXPECT().CreateReminder(gomock.Any(), &pb.CreateReminderReq{
		Reminder: reminder,
	}).Return(reminder, nil)

	if err := testCreateReminder(client); err != nil {
		t.Fatalf("Test failed: %v", err)
	}
}

func testCreateReminder(client pb.ReminderServiceClient) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	_, err := client.CreateReminder(ctx, &pb.CreateReminderReq{
		Reminder: reminder,
	})

	if err != nil {
		return err
	}

	return nil
}
