package main

import (
	"context"
	"fmt"
	"time"

	pb "gitlab.com/michalmajewskiand/grpc-todolist/proto"
	"google.golang.org/grpc"
)

const (
	address = "localhost:50051"
)

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		fmt.Println("did not connect: %v", err)
	}
	defer conn.Close()
	client := pb.NewReminderServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	reminder, err := client.CreateReminder(
		ctx,
		&pb.CreateReminderReq{
			Reminder: &pb.Reminder{
				User:     "Miguelito",
				DueDate:  "2019-07-30T11:29:16Z",
				Finished: false,
				Action:   "It's ALIVE",
			},
		})

	if err != nil {
		fmt.Println("Error: %v", err)
	}

	fmt.Println(reminder)

	return

}
