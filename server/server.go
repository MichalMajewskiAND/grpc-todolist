package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"sync"

	"github.com/google/uuid"
	pb "gitlab.com/michalmajewskiand/grpc-todolist/proto"
	"gitlab.com/michalmajewskiand/grpc-todolist/repository"
	"google.golang.org/grpc"
)

const (
	port = ":50051"
)

//ReminderServiceServer implements the protobuf interface
type ReminderServiceServer struct {
	mutex sync.RWMutex
}

//ListReminders lists all reminders.
func (s *ReminderServiceServer) ListReminders(req *pb.ListRemindersReq, srv pb.ReminderService_ListRemindersServer) error {

	s.mutex.RLock()
	defer s.mutex.RUnlock()

	var reminders []repository.Reminder

	reminders = repository.GetReminders(req.GetUser())

	for _, reminder := range reminders {
		err := srv.Send(&pb.Reminder{
			Id:       reminder.ID,
			User:     reminder.User,
			DueDate:  reminder.DueDate,
			Finished: reminder.Finished,
			Action:   reminder.Action,
		})
		if err != nil {
			fmt.Println(err)
			return err
		}
	}

	return nil
}

//CreateReminder creates new reminder
func (s *ReminderServiceServer) CreateReminder(ctx context.Context, req *pb.CreateReminderReq) (*pb.Reminder, error) {
	reminder := req.GetReminder()

	id, _ := uuid.NewRandom()
	reminder.Id = id.String()

	// data := repository.Reminder{
	// 	ID:       reminder.GetId(),
	// 	User:     reminder.GetUser(),
	// 	DueDate:  reminder.GetDueDate(),
	// 	Finished: reminder.GetFinished(),
	// 	Action:   reminder.GetAction(),
	// }

	//repository.CreateReminder(data)

	return reminder, nil
}

func main() {

	os.Setenv("GRPC_GO_LOG_VERBOSITY_LEVEL", "2")
	os.Setenv("GRPC_GO_LOG_SEVERITY_LEVEL", "info")

	fmt.Println("FOO:", os.Getenv("GRPC_GO_LOG_VERBOSITY_LEVEL"))
	fmt.Println("BAR:", os.Getenv("GRPC_GO_LOG_SEVERITY_LEVEL"))

	lis, err := net.Listen("tcp", port)

	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()

	server := &ReminderServiceServer{}

	pb.RegisterReminderServiceServer(s, server)
	fmt.Println("Server started")

	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

}
