package repository

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

//ReminderRepository interface
type ReminderRepository interface {
	GetReminders(userName string) []Reminder
	CreateReminder(reminder Reminder)
}

//Reminder model
type Reminder struct {
	ID       string `json:"id"`
	User     string `json:"user"`
	DueDate  string `json:"dueDate"`
	Finished bool   `json:"finished"`
	Action   string `json:"action"`
}

// TableName Name of the table
const TableName = "ToDoList"

//GetReminders return all reminders for the user
func GetReminders(userName string) []Reminder {

	config := &aws.Config{
		Region:   aws.String("eu-west-1"),
		Endpoint: aws.String("http://localhost:8000"),
	}
	sess := session.Must(session.NewSession(config))

	svc := dynamodb.New(sess)

	filter := expression.Name("user").Equal(expression.Value(userName))

	expr, err := expression.NewBuilder().WithFilter(filter).Build()
	if err != nil {
		fmt.Println("Got error building expression:")
		fmt.Println(err.Error())
	}

	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		TableName:                 aws.String(TableName),
	}

	result, err := svc.Scan(params)
	if err != nil {
		fmt.Println("Query API call failed:")
		fmt.Println((err.Error()))
	}

	var elements []Reminder

	for _, i := range result.Items {
		item := Reminder{}

		err = dynamodbattribute.UnmarshalMap(i, &item)

		if err != nil {
			fmt.Println("Got error unmarshalling:")
			fmt.Println(err.Error())
		}
		elements = append(elements, item)
	}

	return elements
}

// //CreateReminder inserts new reminder in the table
// func CreateReminder(reminder Reminder) {
// 	item, err := dynamodbattribute.MarshalMap(reminder)
// 	if err != nil {
// 		fmt.Println("Got error marshalling new reminder:")
// 		fmt.Println(err.Error())
// 	}

// 	input := &dynamodb.PutItemInput{
// 		Item:      item,
// 		TableName: aws.String(TableName),
// 	}

// 	_, err = svc.PutItem(input)
// 	if err != nil {
// 		fmt.Println("Got error calling PutItem:")
// 		fmt.Println(err.Error())
// 	}
// }
